//------------------------------------------------------------------------------------
//
//En #inputBusqueda va el id de la etiqueta <input> en la que se buscará.
//En #product-list va el contenedor o div que almacena los elementos (o targetas) que iran desapareciendo con la busqueda.
//En .producto va el elemento que desaparecerá con la busqueda.
//
//-------------------------------------------------------------------------------------
//
// Script de Jquery : <script src="https://code.jquery.com/jquery-3.5.1.slim.js" integrity="sha256-DrT5NfxfbHvMHux31Lkhxg42LY6of8TaYyK50jnxRnM=" crossorigin="anonymous"></script>
//
//-------------------------------------------------------------------------------------
$(document).ready(function () {
    $("#inputBusqueda").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#product-list .producto").filter(function () {
            console.log("hola");
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});