function validar(form) {

    let formulario = document.getElementById(form);

    let input = document.getElementsByTagName("input");

    let submit = true;
    let contador = 0;

    while (submit && contador <= input.length - 1) {
        if (input[contador].value != "") {
            contador++;
        } else {
            alert("El campo " + input[contador].name + " no tiene el formato correcto");
            submit = false;
        }
    }

    if (submit) {
        formulario.submit();
    }



}