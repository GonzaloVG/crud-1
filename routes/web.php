<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layouts.main');
// })->name('main');
Route::view('/', 'main-content')->name('main');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('auth');
//Bolis
Route::get('/bolis', 'BoligrafoController@index')->name('main-boli');
Route::get('/insertarboli', 'BoligrafoController@create')->name('form-boli');
Route::post('/insertarboli', 'BoligrafoController@store')->name('ins-boli');
Route::get('/detalleboli/{id}', 'BoligrafoController@show')->name('show-boli');
Route::get('/actualizarboli/{id}', 'BoligrafoController@edit')->name('edit-boli');
Route::post('/actualizarboli', 'BoligrafoController@update')->name('upd-boli');
Route::get('/delboli/{id}', 'BoligrafoController@destroy')->name('del-boli');


//Usuarios
Route::get('/users', 'UsersController@index')->name('user-index');
Route::get('/users/create', 'UsersController@create')->name('user-create');
Route::post('/users/store', 'UsersController@store')->name('user-store');
Route::get('/users/show/{id}', 'UsersController@show')->name('user-show');
Route::get('users/edit/{id}', 'UsersController@edit')->name('user-edit');
Route::post('users/update/{id}', 'UsersController@update')->name('user-update');
Route::any('users/destroy/{id}', 'UsersController@destroy')->name('user-destroy');


//Tiendas
Route::get('/tiendas', 'TiendaController@index')->name('main-tienda');
Route::get('/insertartienda', 'TiendaController@create')->name('form-tienda');
Route::post('/insertartienda', 'TiendaController@store')->name('ins-tienda');
Route::get('/detalletienda/{id}', 'TiendaController@show')->name('show-tienda');
Route::get('/actualizartienda/{id}', 'TiendaController@edit')->name('edit-tienda');
Route::post('/actualizartienda', 'TiendaController@update')->name('upd-tienda');
Route::get('/deltienda/{id}', 'TiendaController@destroy')->name('del-tienda');
