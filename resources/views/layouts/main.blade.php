<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/icon" href="{{asset('favicon-32x32.png')}}">
    <meta name="description" content="CRUD DE BOLÍGRAFOS BIIC">
    <meta name="author" content="Gonzalo Verdugo">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nova+Square&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/normalize.css')}}">
    <script src="https://kit.fontawesome.com/b228dc2ea7.js" crossorigin="anonymous"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.js"
        integrity="sha256-DrT5NfxfbHvMHux31Lkhxg42LY6of8TaYyK50jnxRnM=" crossorigin="anonymous"></script>
    <script src="{{asset('js/buscador.js')}}"></script>
    <script src="{{asset('js/buscador-users.js')}}"></script>
    <script src="{{asset('js/validate.js')}}"></script>


    <title>Bolígrafos BIIC</title>
</head>

<body>

    <main>
        @include('includes.nav')
        <section>
            @yield('main-section')




        </section>
        @include('includes.footer')

    </main>


</body>

</html>