@extends('layouts.main')


@section('main-section')

<h1>Insercion de bolis</h1>

<form action="{{route('ins-boli')}}" id="insertarBoli" method="post" enctype="multipart/form-data">
    @csrf
    <li><label for="nombre">Nombre</label><input name="nombre" type="text"></li>
    <li><label for="color">Color</label><input name="color" type="text"></li>
    <li><label for="peso">Peso</label><input name="peso" type="text"></li>
    <li><label for="img">Img</label><input name="img" type="file"></li>
    <input class="insert-button" value="Introducir" type="button" onclick="validar('insertarBoli')">
</form>




@endsection