@extends('layouts.main')


@section('main-section')

<h1>Insercion de Tiendas</h1>

<form id="insertarTienda" action="{{route('ins-tienda')}}" method="post">
    @csrf
    <li><label for="nombre">Nombre</label><input name="nombre" type="text"></li>
    <li><label for="calle">Calle</label><input name="calle" type="text"></li>
    <li><label for="numero">Número</label><input name="numero" type="text"></li>
    <li><label for="iframe">Iframe</label><input name="iframe" type="text"></li>
    <li><label for="apertura">Apertura</label><input name="apertura" type="time"></li>
    <li><label for="cierre">Cierre</label><input name="cierre" type="time"></li>
    <li><label for="informacion">Informacion</label><input name="informacion" type="text"></li>


    <input class="insert-button" value="Insertar" type="button" onclick="validar('insertarTienda')">
</form>




@endsection