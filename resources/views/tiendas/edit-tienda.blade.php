@extends('layouts.main')



@section('main-section')

<form action="{{route('upd-tienda')}}" method="post">
    <UL>
        @csrf
        <input type="hidden" name="id" value='{{$tienda->id}}'>
        <li><label for="nombre">Nombre</label><input value="{{$tienda->nombre}}" name="nombre" type="text"></li>
        <li><label for="calle">Calle</label><input value="{{$tienda->calle}}" name="calle" type="text"></li>
        <li><label for="numero">Número</label><input value="{{$tienda->numero}}" name="numero" type="text"></li>
        <li><label for="iframe">Iframe</label><input value="{{$tienda->iframe}}" name="iframe" type="text"></li>
        <li><label for="apertura">Apertura</label><input value="{{$tienda->apertura}}" name="apertura" type="time"></li>
        <li><label for="cierre">Cierre</label><input value="{{$tienda->cierre}}" name="cierre" type="time"></li>
        <li><label for="informacion">Informacion</label><input value="{{$tienda->informacion}}" name="informacion"
                type="text"></li>

        <input type="submit">
    </UL>
</form>

@endsection