@extends('layouts.main')



@section('main-section')


<div class="top-section">
    <h1>Gestión de Usuarios</h1>
    <input placeholder="BUSCAR" type="text" id="inputBusqueda2">
</div>



<div id="users-container">



    @foreach($usuarios as $u)

    <div class="user">
        <p>{{$u->id}}</p>
        <p>{{$u->name}}</p>
        <p>{{$u->email}}</p>
        <p><a href="{{ route('user-show', $u->id)}}">Visualizar</a></p>
        <p><a href="{{ route('user-edit', $u->id)}}">Editar</a></p>
        <p><a href="{{ route('user-destroy', $u->id)}}">Eliminar</a></p>
    </div>

    @endforeach

</div>



@endsection