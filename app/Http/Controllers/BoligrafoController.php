<?php

namespace App\Http\Controllers;

use App\Boligrafo;
use Illuminate\Http\Request;
use DB;

use Storage;

class BoligrafoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $bolis=Boligrafo::selectObjBolis();
        $bolis=Boligrafo::selectObjBolis();
    
        $txt="";

        foreach ($bolis as $boli) {
            $txt.=$boli->imprimirBoli();
        }
      
        

        return view('boli.main-boli', compact('txt'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('boli.form-boli');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $boli = new Boligrafo("", $request['nombre'], $request['color'], $request['peso'], $request['img']->getClientOriginalName());


        // Storage::put("imgs/", $request["img"]);

        // dd($request["img"]);
        // echo $request["img"]->getClientOriginalName();
        // echo gettype($request["img"]);
        Storage::putFileAs("public/imgs", $request["img"], $request['img']->getClientOriginalName());
        $boli->insertar();
        return redirect()->action('BoligrafoController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Boligrafo  $boligrafo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $boli = DB::table('bolis')->where('id', '=', $id)->first();
        

        return view('boli.show-boli', compact('boli'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Boligrafo  $boligrafo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $boli = DB::table('bolis')->where('id', '=', $id)->first();
    
        return view('boli.edit-boli', compact('boli'));
    }
       

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Boligrafo  $boligrafo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Boligrafo::actualizar($request);

        return redirect()->action('BoligrafoController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Boligrafo  $boligrafo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Boligrafo::eliminar($id);
        return redirect()->action('BoligrafoController@index');
    }
}
