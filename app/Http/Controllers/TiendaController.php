<?php

namespace App\Http\Controllers;

use DB;
use App\Tienda;
use Illuminate\Http\Request;

class TiendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //


        $tiendas = Tienda::selectObjTiendas();

        $txt="";
        
        foreach ($tiendas as $tienda) {
            # code...
            $txt.=$tienda->imprimirTienda();
        }

        //
        return view('tiendas.main-tienda', compact('txt'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('tiendas.form-tienda');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $tienda = new Tienda("", $request["nombre"], $request["calle"], $request["numero"], $request["iframe"], $request["apertura"], $request["cierre"], $request["informacion"]);
 
        $tienda->insertar();

 
        // return view('tiendas.user-index');
        return redirect()->action('TiendaController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tienda = DB::table('tiendas')->where('id', '=', $id)->first();
        return view('tiendas.show-tienda', compact('tienda'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tienda = DB::table('tiendas')->where('id', '=', $id)->first();

        return view('tiendas.edit-tienda', compact('tienda'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        // dd($request);
        Tienda::actualizar($request);

        return redirect()->action('TiendaController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tienda::eliminar($id);
        return redirect()->action('TiendaController@index');
    }
}
