<?php


namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Tienda extends Model
{
    private $id;
    private $nombre;
    private $calle;
    private $numero;
    private $iframe;
    private $apertura;
    private $cierre;
    private $informacion;
    

    public function __construct($id="", $nombre="", $calle="", $numero="", $iframe="", $apertura="", $cierre="", $informacion="")
    {
        $this->id=$id;
        $this->nombre=$nombre;
        $this->calle=$calle;
        $this->numero=$numero;
        $this->iframe=$iframe;
        $this->apertura=$apertura;
        $this->cierre=$cierre;
        $this->informacion=$informacion;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id=$id;
    }
    public function getNombre()
    {
        return $this->Nombre;
    }

    public function setNombre($nombre)
    {
        $this->nombre=$nombre;
    }

    public function getCalle()
    {
        return $this->calle;
    }

    public function setCalle($calle)
    {
        $this->calle=$calle;
    }
    public function getNumero()
    {
        return $this->numero;
    }

    public function setNumero($numero)
    {
        $this->numero=$numero;
    }
    public function getIframe()
    {
        return $this->iframe;
    }

    public function setIframe($iframe)
    {
        $this->iframe=$iframe;
    }

    public function getApertura()
    {
        return $this->horario;
    }

    public function setApertura($horario)
    {
        $this->horario=$horario;
    }
    public function getCierre()
    {
        return $this->cierre;
    }

    public function setCierre($cierre)
    {
        $this->cierre=$cierre;
    }

    public function getInformacion()
    {
        return $this->informacion;
    }

    public function setInfomacion()
    {
        $this->informacion= $informacion;
    }

    public function insertar()
    {
        DB::table('tiendas')->insert(
            array(
                "nombre"=>$this->nombre,
                "calle"=>$this->calle,
                "numero"=>$this->numero,
                "iframe"=>$this->iframe,
                "apertura"=>$this->apertura,
                "cierre"=>$this->cierre,
                "informacion"=>$this->informacion
            )
        );
    }

    public static function actualizar($replace)
    {
        DB::table('tiendas')->where('id', $replace['id'])->update(
            array(
                "nombre"=>$replace["nombre"],
                "calle"=>$replace["calle"],
                "numero"=>$replace["numero"],
                "iframe"=>$replace["iframe"],
                "apertura"=>$replace["apertura"],
                "cierre"=>$replace["cierre"],
                "informacion"=>$replace["informacion"]
            )
        );
    }

    public static function eliminar($id)
    {
        DB::table('tiendas')->where('id', '=', $id)->delete();
    }

    public static function selectTiendas()
    {
        $consulta=DB::table('tiendas')->get();

        
        return $consulta;
    }

    public static function selectObjTiendas()
    {
        $sqlTiendas=json_decode(Tienda::selectTiendas(), true);


        
        $arrayTiendas= array();
        foreach ($sqlTiendas as $tienda) {
            $tiendaObj= new Tienda($tienda["id"], $tienda["nombre"], $tienda["calle"], $tienda["numero"], $tienda["iframe"], $tienda["apertura"], $tienda["cierre"], $tienda["informacion"]);
            array_push($arrayTiendas, $tiendaObj);
        }
   
        return $arrayTiendas;
    }

    public function imprimirTienda()
    {
        // $txt="";
        // $txt.="<div class='tienda'>";
     
        // $txt.="<div>";
        // $txt.="<p>".$this->nombre."</p>";
        // $txt.="</div>";
        // $txt.="<div class='product-buttons'><a href='".route('edit-tienda', $this->id)."'>Actualizar</a><a href='".route("del-tienda", $this->id)."'>Eliminar</a></div>";
        // $txt.="</div>";

        $txt="";

        $txt.='<div class="tienda">';
        $txt.='<div class="info-tienda">';
        $txt.='<p>'.$this->nombre.'</p>';
        $txt.='<p>c/'.$this->calle.', '.$this->numero.'</p>';
        $txt.='<p>Abre a las '.$this->apertura.'</p>';
        $txt.='<p>Cierra a las '.$this->cierre.'</p>';
        $txt.='<p>'.$this->informacion.'</p>';
        $txt.='<div class="product-buttons"><a href="'.route('edit-tienda', $this->id).'">Actualizar</a><a href="'.route('del-tienda', $this->id).'">Eliminar</a></div>';
        $txt.='</div>';
        $txt.='<div class="graf-tienda">'.$this->iframe.'</div>';
        $txt.='</div>';
        
  

        return $txt;
    }
}
