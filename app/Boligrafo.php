<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Boligrafo extends Model
{
    //
    private $id;
    private $nombre;
    private $color;
    private $peso;
    private $img;



    public function __construct($id="", $nombre="", $color="", $peso="", $img="")
    {
        $this->id=$id;
        $this->nombre=$nombre;
        $this->color=$color;
        $this->peso=$peso;
        $this->img=$img;
    }

    



    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nombre
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set the value of color
     *
     * @return  self
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get the value of peso
     */
    public function getPeso()
    {
        return $this->peso;
    }

    /**
     * Set the value of peso
     *
     * @return  self
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;

        return $this;
    }

    /**
     * Get the value of img
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Set the value of img
     *
     * @return  self
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }


    

    public function insertar()
    {
        DB::table('bolis')->insert(
            array(
                "nombre"=>$this->nombre,
                "color"=>$this->color,
                "peso"=>$this->peso,
                "img"=>$this->img
            )
        );
    }

    public static function actualizar($replace)
    {
        DB::table('bolis')->where('id', $replace['id'])->update(
            array(
                "nombre"=>$replace['nombre'],
                "color"=>$replace['color'],
                "peso"=>$replace['peso'],
                "img"=>$replace['img']
            )
        );
    }

    public static function eliminar($id)
    {
        DB::table('bolis')->where('id', '=', $id)->delete();
    }

    public static function selectBolis()
    {
        $consulta=DB::table('bolis')->get();

        
        return $consulta;
    }


    public static function selectObjBolis()
    {
        $sqlBolis=json_decode(Boligrafo::selectBolis(), true);


        
        $arrayBolis= array();
        foreach ($sqlBolis as $boli) {
            $boliOBJ= new Boligrafo($boli['id'], $boli['nombre'], $boli['color'], $boli['peso'], $boli['img']);
            array_push($arrayBolis, $boliOBJ);
        }
        // echo gettype($sqlBolis);

       

        // while ($fila = mysql_fetch_array($sqlBolis, MYSQL_BOTH)) {
        //     $boli = new Boligrafo($fila["id"], $fila["nombre"], $fila["color"], $fila["peso"], $fila["img"]);
        //     array_push($arrayBolis, $boli);
        // }
        return $arrayBolis;
    }


    public function imprimirBoli()
    {

        // <div class="producto">
        // <div>
        // <img src="" alt="">
        // </div>
        // <div>
        // <p>boli 1</p>
        // </div>
        
        // </div>

        $txt="<a href='".route("show-boli", $this->id)."'>";
        $txt.="<div class='producto'>";
        $txt.="<div>";
        $txt.="<img src=".asset("storage/imgs/".$this->img).">";
        $txt.="</div>";
       
        $txt.="<div class='product-buttons'><p>".$this->nombre."</p><a href='".route('edit-boli', $this->id)."'>Actualizar</a><a href='".route("del-boli", $this->id)."'>Eliminar</a></div>";
        $txt.="</div>";
        $txt.="</a>";
        



        // $txt="<tr>";
        // $txt.="<td>".$boli->nombre."</td>";
        // $txt.="<td>".$boli->color."</td>";
        // $txt.="<td>".$boli->peso."</td>";
        // $txt.="<td><img class='img-table' src=".asset("storage/imgs/".$boli->img)."></td>";
        // $txt.="<td><a href=";
        // $txt.=route('show-boli', $boli->id);
        // $txt.=">VISUALIZAR BOLI</a></td>";
        // $txt.="<td><a href=";
        // $txt.=route('edit-boli', $boli->id);
        // $txt.=">EDITAR BOLI</a></td>";
        // $txt.="<td><a style='color:red' href=";
        // $txt.=route('del-boli', $boli->id);
        // $txt.=">BORRAR BOLI</a></td>";
        // $txt.="</tr>";

    

        return $txt;
    }
}
